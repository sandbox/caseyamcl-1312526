<?php

/**
 * @file
 * This is the server file for Biblio unAPI, a module that
 * extends the biblio output, and adds an unAPI server.
 *
 * @author Casey McLaughlin
 * @package Biblio unAPI for Drupal
 */

//------------------------------------------------------------------------

//ID and Format will be in the $_GET array
$id = (isset($_GET['id'])) ? $_GET['id'] : FALSE;
$format = (isset($_GET['format'])) ? $_GET['format'] : FALSE;

//Validate input

//Format must be in the format array
if ($format && ! in_array($format, biblio_unapi_get_all_available_formats(TRUE)))
    biblio_unapi_death('406');

//The id must be a positive integer
if ($id && ((int) $id != $id OR (int) $id <= 0))
  biblio_unapi_death('400');

//Route!
if ($id && $format)
  $xml_out = biblio_unapi_get_record_xml($id, $format);
elseif ($id)
  $xml_out = biblio_unapi_get_supported_formats_xml($id);
elseif ($format)
  death('400');
else
  $xml_out = biblio_unapi_get_supported_formats_xml();

//Output
header('Content-type: application/xml; charset="utf-8"');
echo $xml_out;

//------------------------------------------------------------------------

/**
 * Generate XML output for a biblio node.
 *
 * Formats a node using the speicifed format.
 *
 * @param int $id
 *  The id of the node to output; must be a biblio node
 *
 * @param string $format
 *  The format to use.  Must be a valid format
 *
 * @return string
 *  XML output
 */
function biblio_unapi_get_record_xml($id, $format) {
  $node = biblio_unapi_get_node_from_db($id);

  if ( ! $node)
    biblio_unapi_death('404');

  switch ($format) {
    case 'srw_dc': return biblio_unapi_output_srw_dc($id, $node); break;
    case 'mods': return biblio_unapi_output_mods($id, $node); break;
  }
}

//------------------------------------------------------------------------

/**
 * Convert a biblio node into MODS XML.
 *
 * @param int $id
 *  The id of the node to convert
 *
 * @param type $node
 *  The node record, called from Drupal function node_load()
 *
 * @return string
 *  XML output
 */
function biblio_unapi_output_mods($id, $node) {
  //debugging; dump($node);

  $out_xml  = new SimpleXMLElement('<mods xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.loc.gov/mods/v3" version="3.0" xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-0.xsd"></mods>');

  //Title
  $title_info = $out_xml->addChild('titleInfo');
  $title_info->addChild('title', xml_prep(xml_prep($node->title)));

  //Authors
  foreach ($node->biblio_contributors as $author) {
    $name = $out_xml->addChild('name');
    $name->addAttribute('type', 'personal');

    $role = $name->addChild('role');
    $rt = $role->addChild('roleTerm', xml_prep(xml_prep('author')));
    $rt->addAttribute('type', 'text');
    $rt->addAttribute('authority', 'marcrelator');

    $rt = $role->addChild('roleTerm', xml_prep(xml_prep('aut')));
    $rt->addAttribute('type', 'code');
    $rt->addAttribute('authority', 'marcrelator');

    if ( ! empty($author['lastname']) && ( ! empty($author['firstname']) OR ! empty($author['initials']))) {
      $np = $name->addChild('namePart', xml_prep(xml_prep($author['lastname'])));
      $np->addAttribute('type', 'family');

      if (drupal_strlen($author['firstname']) > 0) {
        $np = $name->addChild('namePart', xml_prep(xml_prep($author['firstname'])));
        $np->addAttribute('type', 'given');
      }
      elseif (drupal_strlen($author['initials']) > 0) {
        $np = $name->addChild('namePart', xml_prep(xml_prep($author['initials'])));
        $np->addAttribute('type', 'given');
      }
    }
    else
      $name->addChild('namePart', xml_prep(xml_prep($author['name'])));
  }

  //Origin
  $oi = $out_xml->addChild('originInfo');
  $oi->addChild('issuance', xml_prep(xml_prep('monographic')));

  if ( ! empty($node->biblio_publisher))
    $oi->addChild('publisher', xml_prep(xml_prep($node->biblio_publisher)));

  if ( ! empty($node->biblio_place_published)) {
    $place = $oi->addChild('place');
    $pt = $place->addChild('placeTerm', xml_prep($node->biblio_place_published));
    $pt->addAttribute('type', 'text');
  }

  if ( ! empty($node->biblio_date))
    $oi->addChild('dateIssued', xml_prep($node->biblio_date));
  elseif ( ! empty($node->biblio_year))
    $oi->addChild('dateIssued', xml_prep($node->biblio_year));

  if ( ! empty($node->biblio_year)) {
    $cd = $oi->addChild('copyrightDate', xml_prep($node->biblio_year));
    $cd->addAttribute('encoding', 'w3cdtf');
    $cd->addAttribute('keyDate', 'yes');
    $cd->addAttribute('qualifier', 'inferred');
  }

  if ( ! empty($node->biblio_edition))
    $oi->addChild('eddition', xml_prep($node->biblio_edition));

  //Language
  if ( ! empty($node->biblio_lang)) {
    $lng = $out_xml->addChild('language');
    $lt = $lng->addChild('languageTerm', xml_prep($node->biblio_lang));
    $lt->addAttribute('type', 'code');
    $lt->addAttribute('authority', 'iso639-2b');
  }

  //Parent Journal (stored in biblio_secondary_title)
  if ($node->biblio_type_name == 'Journal Article' && drupal_strlen($node->biblio_secondary_title) > 0) {
    $ri = $out_xml->addChild('relatedItem');
    $ri->addAttribute('type', 'host');
    $ri->addChild('titleInfo')->addChild('title', xml_prep($node->biblio_secondary_title));
    $ri->addChild('originInfo')->addChild('issuance', xml_prep('continuing'));

    $pt = $ri->addChild('part');
    if ( ! empty($node->biblio_volume)) {
      $detail = $pt->addChild('detail');
      $detail->addAttribute('type', 'volume');
      $detail->addChild('number', xml_prep($node->biblio_volume));
    }

    if ( ! empty($node->biblio_issue)) {
      $detail = $pt->addChild('detail');
      $detail->addAttribute('type', 'issue');
      $detail->addChild('number', xml_prep($node->biblio_issue));
    }

    if ( ! empty($node->biblio_pages) && strpos($node->biblio_pages, '-')) {
      list($start, $end) = explode('-', $node->biblio_pages, 2);

      $extent = $pt->addChild('extent');
      $extent->addAttribute('unit', 'pages');
      $extent->addChild('start', xml_prep($start));
      $extent->addChild('end', xml_prep($end));
    }

    if ( ! empty($node->biblio_year))
      $pt->addChild('date', xml_prep($node->biblio_year));
  }

  //Identifiers
  if ( ! empty($node->biblio_doi))
    $out_xml->addChild('identifier', xml_prep($node->biblio_doi))->addAttribute('type', 'doi');

  if ( ! empty($node->biblio_issn))
    $out_xml->addChild('identifier', xml_prep($node->biblio_issn))->addAttribute('type', 'issn');

  if ( ! empty($node->biblio_isbn))
    $out_xml->addChild('identifier', xml_prep($node->biblio_isbn))->addAttribute('type', 'isbn');

  if ( ! empty($node->biblio_url))
    $out_xml->addChild('identifier', xml_prep($node->biblio_url))->addAttribute('type', 'uri');

  //Keywords
  foreach ($node->biblio_keywords as $kw) {
    $out_xml->addChild('subject', xml_prep($kw));
  }

  //Absract
  if ( ! empty($node->biblio_abst_e))
    $out_xml->addChild('abstract', xml_prep($node->biblio_abst_e));

  //Type of resrouce
  $special_type_mappings = array(
    'Film' => 'moving image',
    'Broadcast' => 'moving image',
    'Audiovisual' => 'moving image',
    'Artwork' => 'still image',
    'Chart' => 'still image',
    'Map' => 'cartographic',
    'Miscellaneous' => 'mixed material',
    'Miscellaneous Section' => 'mixed material',
    'Software' =>  'mixed material'
  );

  if (in_array($node->biblio_type_name, array_keys($special_type_mappings)))
    $out_xml->addChild('typeOfResource', xml_prep($special_type_mappings[$node->biblio_type_name]));
  else
    $out_xml->addChild('typeOfResource', xml_prep('text'));

  //Link to this record in our biblio!
  $out_xml->addChild('location')->addChild('url', xml_prep(url('node/' . $node->nid, array('absolute' => TRUE))));

  //All done!
  return $out_xml->asXML();
}

//------------------------------------------------------------------------

/**
 * Convert a biblio node into Dublic Core XML
 *
 * @param int $id
 *  The id of the node to convert
 *
 * @param type $node
 *  The node record, called from Drupal function node_load()
 *
 * @return string
 *  XML output
 */
function biblio_unapi_output_srw_dc($id, $node) {
  $out_xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $out_xml .= "<oai_dc:dc xmlns:oai_dc=\"http://www.openarchives.org/OAI/2.0/oai_dc/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd\">";
  $out_xml .= "<dc:identifier>" . url('node/' . $id) . "</dc:identifier>";
  $out_xml .= "<dc:title>$node->title</dc:title>";
  $out_xml .= "<dc:type>text</dc:type>";

  foreach ($node->biblio_contributors as $bc)
    $out_xml .= "<dc:creator>" . $bc['name'] . "</dc:creator>";

  $out_xml .= "<dc:publisher>" . $node->biblio_publisher . "</dc:publisher>";
  $out_xml .= "<dc:date>" . date('r', strtotime($node->biblio_date)) . "</dc:date>";
  $out_xml .= "<dc:format>application/xml</dc:format>";
  $out_xml .= "<dc:language>" . $node->language . "</dc:language>";
  $out_xml .= "<dc:description>" . $node->biblio_abst_e . "</dc:description></oai_dc:dc>";

  return $out_xml;
}

//------------------------------------------------------------------------

/**
 * Return an XML string of supported unAPI output formats
 *
 * Optionally, this function accepts a node ID.  The function may
 * then check to see which types of nodes support which formats.  Currently,
 * this functionality does not exist.  So, this function returns all installed
 * formats for all ids.
 *
 * @param int $id NULL
 *  The ID of a node to check format types against
 *
 * @return string
 *  A string for Biblio unAPI output
 */
function biblio_unapi_get_supported_formats_xml($id = NULL) {
  $formats_tag = ( ! is_null($id)) ? "<formats id='$id'>" : "<formats>";

  return $formats_tag . implode(biblio_unapi_get_all_available_formats()) . '</formats>';
}

//------------------------------------------------------------------------

/**
 * Retrieve all available formats
 *
 * @param bool $format_id_only FALSE
 *  If TRUE, this function will return only the array keys
 *
 * @return array
 *  Array of supported formats
 */
function biblio_unapi_get_all_available_formats($format_id_only = FALSE) {
  $avail_formats = array();
  $avail_formats['srw_dc'] = '<format name="srw_dc" type="application/xml" docs="http://www.loc.gov/standards/sru/dc-schema.xsd" />';
  $avail_formats['mods'] = '<format name="mods" type="application/xml" docs="http://www.loc.gov/standards/mods/" />';

  return ($format_id_only) ? array_keys($avail_formats) : $avail_formats;
}

//------------------------------------------------------------------------

/**
 * Halt execution of the unAPI interface, and print an error.
 *
 * This function coforms to the unAPI Recommended HTTP  status codes
 * and error messages, and should be used for handling exceptional requests
 * or server issues.
 * @link http://unapi.info/specs/
 *
 * @param type $response_code
 * @param type $msg
 */
function biblio_unapi_death($response_code = '401', $msg = NULL) {
  $response_strings = array();
  $response_strings[400] = 'Bad Request';
  $response_strings[401] = 'Not Authorized';
  $response_strings[404] = 'Not Found';
  $response_strings[406] = 'Not Acceptable';

  //Translate t() function here
  $msg = ($msg) ? t($response_strings[$response_code] . ': ' . $msg) : t($response_strings[$response_code]);

  header('HTTP/1.0 ' . $response_code . ' ' . $msg);
  echo $response_code . ' ' . $msg;
  exit();
}

//------------------------------------------------------------------------

/**
 * Retrieve a node from the database using the node_load() drupal API function
 *
 * If the node is not a biblio node, execution will halt.
 *
 * @param int $id
 *  ID of the node to be retrieved
 *
 * @return object
 *  A node object
 */
function biblio_unapi_get_node_from_db($id) {
  $node = node_load($id);
  
  //If not biblio, we die...
  if (strcmp($node->type, 'biblio') !== 0)
    biblio_unapi_death('400', 'Nodetype mismatch');

  //If we don't have permission, we die...
  if ( ! node_access('view', $node))
    biblio_unapi_death('401');
  
  return $node;
}

//------------------------------------------------------------------------

/**
 * Prepare XML output
 *
 * @param string $str
 *  Valid XML that requires prepping of special characters
 *
 * @return string
 *  The prepped XML
 */
function xml_prep($str) {
  //Fix ampersands
  $str = str_replace("&", "&amp;", $str);

  return $str;
}

//------------------------------------------------------------------------

/**
 * Debug helper var_dump wrapper
 *
 * Simply sends a text/plain header and performs a var_dump.  This is just
 * a shortcut function for quick and dirty debugging.
 *
 * @param mixed $data
 *  Anything to be var-dumped
 */
function dump($data) {

  header("Content-type: text/plain");
  var_dump($data);
  exit();

}

/* EOF: unapi_server.php */