Biblio-unAPI
=============

This is a Biblio plug-in module which implements the unAPI protocol to provide
richer representation of metadata than the built-in COInS data provides.  This
allows Biblio records to export more easily into Zotero and other tools that can 
speak to unAPI servers.

Currently, this module only outputs in MODS and Dublin Core, but more data
formats may be added in the future.


Prerequisite software
=====================

This module requires Drupal 7 and Biblio v7.x-1.0-beta4 or newer


Using the "Biblio unAPI" module
====================================

No configuration is necessary.  Just install it, enable it, and it's ready to go


Roadmap
=======

- Add option for enabling or disabling COInS metadata (currently disable is
  hard-coded)
- Add new datatypes in the unAPI server